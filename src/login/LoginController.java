package login;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.controlsfx.control.Notifications;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
public class LoginController  {
	public static String docId;

	@FXML
	private TextField mobileno;
	
	@FXML
	private PasswordField password;
	@FXML
	private Button loginButton,closeButton;
	@FXML
	private Label label;
	@FXML
	private AnchorPane anchor;
	
	@FXML
	public void login(ActionEvent event) throws Exception {
		if((!(mobileno.getText()).isEmpty())&&(!(password.getText()).isEmpty())) {
			MongoClient mongoClient = new MongoClient("localhost",27017); 
			DB db =mongoClient.getDB("PLocalDB");//get the existing database DBCollection table=
			DBCollection table=db.getCollection("Doctor");			  
			System.out.println("Connected");
			
			DBObject ob = new BasicDBObject();
			ob.put("Mobile",mobileno.getText());
			BasicDBObject bo=new BasicDBObject();
			DBCursor dbc=table.find(ob);
			if (dbc.hasNext()) {
				bo = (BasicDBObject) dbc.next();
				System.out.println(bo.getString("Mobile"));
				docId=bo.getString("_id");
				}
				
			if((mobileno.getText().equals(bo.getString("Mobile")))&&(password.getText().equals(bo.getString("Password")))) {
				Stage stage=(Stage)loginButton.getScene().getWindow();
				stage.close();
			//----------------------
				
				String mobile_no=bo.getString("Mobile");
				String name=bo.getString("Name");
				String password=bo.getString("Password");
				
				//---------------------------REST
				/*
				 * URL url= new URL(""); HttpsURLConnection
				 * con=(HttpsURLConnection)url.openConnection(); con.setRequestMethod("GET");
				 * BufferedReader in=new BufferedReader(new
				 * InputStreamReader(con.getInputStream())); StringBuffer sb=new StringBuffer();
				 * String line; while ((line =in.readLine()) !=null) {
				 * 
				 * sb.append(line);
				 * 
				 * } in.close(); System.out.println(sb.toString());
				 */
			//-------------------
			Stage primaryStage=new Stage();
			primaryStage.initStyle(StageStyle.UNDECORATED);

			//loadSpalsh();
			VBox root = FXMLLoader.load(getClass().getResource("/firstPage/FirstPage.fxml"));
			
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			//scene.setRoot(root);
			
			primaryStage.setScene(scene);
			primaryStage.show();
			 //------------- 
					Image img= new Image("/img/tick.png");
					Notifications notyf=Notifications.create()
					.title("")
					.text("Login  Successfully..")
					.graphic(new ImageView(img))
					.hideAfter(Duration.seconds(1))
					.position(Pos.TOP_RIGHT); 
					notyf.darkStyle();
					notyf.show();
		}else {		
			
					Image img= new Image("/img/Alert1.png");
					Notifications notyf=Notifications.create()
					.title("Error")
					.text("Please Contact HelpDesk")
					.graphic(new ImageView(img))
					.hideAfter(Duration.seconds(3))
					.position(Pos.TOP_RIGHT); 
					notyf.darkStyle();
					notyf.show();
			}
			
		}
		
		else {
			
			label.setText("please fill out all");
		}
	}
	
	//-------------- otp generation---------
	@FXML
    void doctorRegistration(ActionEvent event) {
    	
    	Stage primaryStage=new Stage();
		primaryStage.initStyle(StageStyle.UNDECORATED);
		AnchorPane root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/doctorRegistration/Otp.fxml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	
	@FXML
	void closeWindow(ActionEvent e) {
		Stage stage=(Stage)closeButton.getScene().getWindow();
		stage.close();
	}
	

	
}
