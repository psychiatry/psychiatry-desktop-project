package login;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WelcomeController implements Initializable{
	@FXML
	private StackPane rootpane;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		new Splash().start();
	}
	
	
	class Splash extends Thread{
		@Override
		public void run() {
			try {
						Thread.sleep(2000);
						Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								Parent root=null;
								try {
									root = FXMLLoader.load(getClass().getResource("/login/Login.fxml"));
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								Stage primaryStage = new Stage();

								primaryStage.initStyle(StageStyle.UNDECORATED);
								Scene scene = new Scene(root);
								//scene.setRoot(root);
								//primaryStage.initStyle(StageStyle.UNDECORATED);

								primaryStage.setScene(scene);
								primaryStage.show();
								rootpane.getScene().getWindow().hide();
								// TODO Auto-generated method stub
								
							}
						});
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
	}


