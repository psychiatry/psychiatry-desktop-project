package patient;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;

import org.bson.types.ObjectId;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import firstPage.FirstPageController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class EditDetailsController implements Initializable {

    @FXML
    private JFXTextField pName,pGender,pMobile,pEmployment,pHeight,pWeight,bName,relation,bMobile,symptoms,diagnosis,pAddress,Taluk,pPO,pVillage,pDistrict;
    @FXML
    private JFXButton updateBtn;
    @FXML
    private Button closeButton;

    @FXML
    private Label date;

    @FXML
    private Label time;
    @FXML
    private JFXDatePicker nextDate;
    @FXML
    private JFXTimePicker nextTime;

    
    MongoClient mongoClient = new MongoClient("localhost", 27017);
	DB db = mongoClient.getDB("PLocalDB");// get the existing database DBCollection table=
	DBCollection table = db.getCollection("Patient");
	
	
	// -----------Close---------------------------
    @FXML
    void closeWindow(ActionEvent event) {
    	Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
    }
    
    
    @Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		System.out.println("Connected");

		DBObject ob = new BasicDBObject();
		ob.put("_id", new ObjectId(FirstPageController.id));
		BasicDBObject bo = new BasicDBObject();
		DBCursor dbc = table.find(ob);
		if (dbc.hasNext()) {
			bo = (BasicDBObject) dbc.next();
			pMobile.setText(bo.getString("Mobile No"));
			pName.setText(bo.getString("Name"));
			pAddress.setText(bo.getString("House Name"));
			diagnosis.setText(bo.getString("Diagnosis"));
			pDistrict.setText(bo.getString("District"));
			pPO.setText(bo.getString("PO"));
			pVillage.setText(bo.getString("Village"));
			pWeight.setText(bo.getString("Weight"));
			pHeight.setText(bo.getString("Height"));
			bName.setText(bo.getString("ByName"));
			bMobile.setText(bo.getString("ByMobile"));
			relation.setText(bo.getString("ByRelation"));
			Taluk.setText(bo.getString("Taluk"));
			pEmployment.setText(bo.getString("Employment"));
			symptoms.setText(bo.getString("symptoms"));
			date.setText(bo.getString("NextConsultationDate"));			
			time.setText(bo.getString("NextConsultationTime"));

		}
    }
    
    
    @FXML
    void updateDetails(ActionEvent event) {
    	System.out.println("in Update");
		BasicDBObject doc = new BasicDBObject();
		DBObject ob = new BasicDBObject();
		ob.put("_id", new ObjectId(FirstPageController.id));
		DBCursor dbc = table.find(ob);
		System.out.println(nextDate.getValue());
		
		if(nextDate.getValue()!=null&&(nextTime.getValue()!=null)){
		LocalDate date=nextDate.getValue();
		LocalTime time=nextTime.getValue();
		doc.put("NextConsultationTime",time.toString() );	
		doc.put("NextConsultationDate", date.toString());	
		}
		
		doc.put("Mobile No", pMobile.getText());
		doc.put("Weight", pWeight.getText());
		doc.put("Height", pHeight.getText());
		doc.put("ByRelation", relation.getText());
		doc.put("Employment",pEmployment .getText());
		doc.put("symptoms", symptoms.getText());
		doc.put("Name", pName.getText());
		doc.put("Diagnosis", diagnosis.getText());
		doc.put("ByName", bName.getText());
		doc.put("ByMobile", bMobile.getText());
		doc.put("House Name",pAddress .getText());
		doc.put("Taluk", Taluk.getText());
		doc.put("Village", pVillage.getText());
		doc.put("District", pDistrict.getText()); 
		doc.put("PO", pPO.getText());	
		

		table.update(ob, new BasicDBObject("$set", doc));
		System.out.println("in Update");

		Stage stage = (Stage) closeButton.getScene().getWindow();

		stage.close();
    }

    

}
