package patient;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ResourceBundle;

import org.bson.types.ObjectId;

import com.jfoenix.controls.JFXButton;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import firstPage.FirstPageController;
import firstPage.FirstPageController.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.control.Label;

import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class PatientController implements Initializable {
	
	@FXML
	JFXButton updateBtn;
	@FXML
	private Tab profile;
	@FXML
	private Label name;
	@FXML
	private Label gender;
	@FXML
	private Label mobile;
	@FXML
	private Label emp;
	@FXML
	private Label mStatus;
	@FXML
	private Label height;
	@FXML
	private Label bystander;
	@FXML
	private Label relation;
	@FXML
	private Label diagnosis;
	@FXML
	private Label age;
	@FXML
	private Label blood;
	@FXML
	private Label education;
	@FXML
	private Label annualIncome;
	@FXML
	private Label children;
	@FXML
	private Label weight;
	@FXML
	private Label mob;
	@FXML
	private Label symptoms;
	@FXML
	private Label Address;
	@FXML
	private Label Taluk;
	@FXML
	private Label po;
	@FXML
	private Label village,date,time;
	@FXML
	private Label district;
	@FXML
	private Label pin;
	@FXML
	private Label landline;
	@FXML
	private Label street;
	@FXML
	private Tab records;
	@FXML
	private TextField serchbox;
	@FXML
	private Button recordSearch;
	
	@FXML
	private Tab consultations;
	@FXML
	private TextField serchbox1;
	@FXML
	private Button consultSearch;
	@FXML
	private TableView recordsTable;
	@FXML
	private TableView consultationTable;
	@FXML
	private TableView rxTable;
	@FXML
    private TableColumn<?, ?> colMedicineName;

    @FXML
    private TableColumn<?, ?> colDosage,colDuration;

    @FXML
    private TableColumn<?, ?> colFrequency;
    @FXML
    private TableColumn<?, ?> colDate,colSlNo;
    @FXML
    private TableColumn<?, ?> colFood,colM_id;
    
	@FXML
	private Tab rx;
	@FXML
	private TextField serchbox2;
	@FXML
	private Button search2;
	@FXML
	private Tab advices;
	@FXML
	private Tab summary;
	@FXML
	private JFXButton newBtn;
	@FXML
	private JFXButton edit;
	@FXML
	private JFXButton delete;
	@FXML
	private JFXButton submit;
	@FXML
	private Button closeButton;
	
	DateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
	String id=FirstPageController.id;
	private final ObservableList<Medicine> list= FXCollections.observableArrayList();
	MongoClient mongoClient = new MongoClient("localhost",27017); 
	DB db =mongoClient.getDB("PLocalDB");//get the existing database DBCollection table=
	DBCollection table=db.getCollection("Patient");	
	DBCollection rxColl=db.getCollection("Medicine");

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		System.out.print(id);
		  
		  
		System.out.println("Connected");
		
		DBObject ob = new BasicDBObject();
		ob.put("_id", new ObjectId(id));
		BasicDBObject bo=new BasicDBObject();
		DBCursor dbc=table.find(ob);
		if (dbc.hasNext()) {
            bo = (BasicDBObject) dbc.next();
            System.out.print(bo.getString("Name"));
            name.setText(bo.getString("Name"));
            gender.setText(bo.getString("Gender"));
            age.setText(bo.getString("Age"));
            mobile.setText(bo.getString("Mobile No"));
            emp.setText(bo.getString("Employment"));
            weight.setText(bo.getString("Weight"));
            blood.setText(bo.getString("BloodGroup"));
            height.setText(bo.getString("Height"));
            mStatus.setText(bo.getString("MaritalStatus"));
            symptoms.setText(bo.getString("symptoms"));
            bystander.setText(bo.getString("ByName"));
            mob.setText(bo.getString("ByMobile"));
            relation.setText(bo.getString("ByRelation"));
            Address.setText(bo.getString("House Name"));
            Taluk.setText(bo.getString("Taluk")); 
            pin.setText(bo.getString("Pin")); 
            district.setText(bo.getString("District"));
            annualIncome.setText(bo.getString("Annuel Income"));
            diagnosis.setText(bo.getString("Diagnosis"));
            po.setText(bo.getString("PO"));
            time.setText(bo.getString("NextConsultationTime"));            
            date.setText(bo.getString("NextConsultationDate"));

		}
		
		
		////-----------RX--------------
		DBObject idOb = new BasicDBObject();
		BasicDBObject store= new BasicDBObject();
		idOb.put("p_id",new ObjectId(id));
		DBCursor dbcr=rxColl.find(idOb);
		int j=1;
		while(dbcr.hasNext()){
			store=(BasicDBObject) dbcr.next();
			System.out.println(store.getString("Duration"));
			list.add(new Medicine(j++,formater.format(store.getDate("PrescriptionDate")), store.getString("MedicineName"), store.getString("Dosage")+" ml", store.getString("Frequency"),store.getString("Duration"), store.getString("FoodConnection"),store.getObjectId("_id")));
		}
		colSlNo.setCellValueFactory(new PropertyValueFactory<>("slNo"));
		colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
		colMedicineName.setCellValueFactory(new PropertyValueFactory<>("medicineName"));
		colDosage.setCellValueFactory(new PropertyValueFactory<>("dosage"));
		colFrequency.setCellValueFactory(new PropertyValueFactory<>("frequency"));
		colDuration.setCellValueFactory(new PropertyValueFactory<>("duration"));
		colFood.setCellValueFactory(new PropertyValueFactory<>("foodConnection"));
		colM_id.setCellValueFactory(new PropertyValueFactory<>("m_id"));

		rxTable.setItems(list);
		
		
	}
	public static ObjectId id1;
	//---------------------
	@FXML
	public void editMedicine(ActionEvent e) {
		id1=((Medicine) rxTable.getSelectionModel().getSelectedItem()).getM_id();
		System.out.println(id1);
		Stage primaryStage=new Stage();
		primaryStage.initStyle(StageStyle.UNDECORATED);
		//loadSpalsh();
		AnchorPane root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/medicine/UpdateMedicine.fxml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	//--------------------Close---------------------------
    
		@FXML
		void closeWindow(ActionEvent e) {
			Stage stage=(Stage)closeButton.getScene().getWindow();
			stage.close();
		}
	//-------------------Patient Details updation----------------
	@FXML
	void updateWindow(ActionEvent e) {
		Stage primaryStage=new Stage();
		primaryStage.initStyle(StageStyle.UNDECORATED);
		//loadSpalsh();
		AnchorPane root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/patient/EditDetails.fxml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	@FXML
	public void refresh() {
		rxTable.getItems().removeAll(list); 
		initialize(null, null);
		
	}
	@FXML
    void openEditWindow(ActionEvent e) {
    	Stage primaryStage = new Stage();
		primaryStage.initStyle(StageStyle.UNDECORATED);
		// loadSpalsh();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/medicine/AddMedicine.fxml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
    }
	
	//-----------deletion----------
	@FXML
	void deleteMedicine(ActionEvent e) {
		id1=((Medicine) rxTable.getSelectionModel().getSelectedItem()).getM_id();

		BasicDBObject qry=new BasicDBObject();
		qry.put("_id",PatientController.id1);
		DBCursor bo=rxColl.find(qry);
		BasicDBObject bdbo=(BasicDBObject) bo.next();
		rxColl.remove(bdbo);			
		rxTable.getItems().removeAll(list); 
			initialize(null, null);

		
	}
	public static class Medicine{
		int slNo;
		String medicineName;
		String dosage;
		String frequency;
		String foodConnection;
		String duration;
		String date;
		ObjectId m_id;
		
		
		public Medicine(int slNo,String date2,String medicineName, String dosage, String frequency,String dur , String foodConnection,ObjectId id) {
			super();
			this.medicineName = medicineName;
			this.dosage = dosage;
			this.frequency = frequency;
			this.foodConnection = foodConnection;
			this.date = date2;
			this.slNo=slNo;
			this.duration=dur;
			this.m_id=id;
			
		}
		public ObjectId getM_id() {
			return m_id;
		}
		public void setM_id(ObjectId m_id) {
			this.m_id = m_id;
		}
		public String getDuration() {
			return duration;
		}
		public void setDuration(String duration) {
			this.duration = duration;
		}
		public String getMedicineName() {
			return medicineName;
		}
		public int getSlNo() {
			return slNo;
		}
		public void setSlNo(int slNo) {
			this.slNo = slNo;
		}
		public void setMedicineName(String medicineName) {
			this.medicineName = medicineName;
		}
		public String getDosage() {
			return dosage;
		}
		public void setDosage(String dosage) {
			this.dosage = dosage;
		}
		public String getFrequency() {
			return frequency;
		}
		public void setFrequency(String frequency) {
			this.frequency = frequency;
		}
		public String getFoodConnection() {
			return foodConnection;
		}
		public void setFoodConnection(String foodConnection) {
			this.foodConnection = foodConnection;
		}
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		
	}
	
}
