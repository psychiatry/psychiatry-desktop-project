package patient;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.bson.types.ObjectId;

import com.jfoenix.controls.JFXButton;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.control.TableColumn;

public class FullListController implements Initializable {
	@FXML
	private TableView fulllist;
	@FXML
	private TableColumn slNo;
	@FXML
	private TableColumn name;
	@FXML
	private TableColumn date;
	@FXML
	private TableColumn time,p_id;
	@FXML
	private Button closeButton;
	@FXML
	private JFXButton reSchedule;
	@FXML
	private JFXButton remove;
	@FXML
	private JFXButton refresh;
	public static ObjectId _id;
	int i=1;
	MongoClient mongoClient = new MongoClient("localhost", 27017);
	DB db = mongoClient.getDB("PLocalDB");// get the existing database DBCollection table=
	DBCollection rxtable = db.getCollection("Patient");   
	private final ObservableList<Patient> list= FXCollections.observableArrayList();
	int sl_no=1;
	
	
	
	// Event Listener on Button[#closeButton].onAction
	@FXML
	public void closeWindow(ActionEvent event) {
		Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
		
	}
	
	
	
	@FXML
	void reScheduleWindow(ActionEvent e) {
		_id=((Patient) fulllist.getSelectionModel().getSelectedItem()).getP_id();

		Stage stage=new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/patient/DateTime.fxml"));
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		Scene scene = new Scene(root);
		//scene.setRoot(root);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setScene(scene);
		stage.show();
	}
	
	
	// Event Listener on JFXButton[#refresh].onAction
	@FXML
	public void refresh(ActionEvent event) {
		fulllist.getItems().removeAll(list); 
		sl_no=1;
		initialize(null, null);
		
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		  BasicDBObject bo= new BasicDBObject();
		  DBCursor cursor=rxtable.find();
		  while (cursor.hasNext()) {
			  System.out.println(i++);
			  bo=(BasicDBObject) cursor.next();
			  list.add(new Patient(sl_no++,bo.getString("Name"),bo.getString("NextConsultationDate") ,bo.getString("NextConsultationTime") ,bo.getObjectId("_id") ));
		}
		  slNo.setCellValueFactory(new PropertyValueFactory<>("slNo"));
		  name.setCellValueFactory(new PropertyValueFactory<>("name"));
		  date.setCellValueFactory(new PropertyValueFactory<>("date"));
		  time.setCellValueFactory(new PropertyValueFactory<>("time"));
		  p_id.setCellValueFactory(new PropertyValueFactory<>("p_id"));
		  
		  fulllist.setItems(list);

		
		
		
	}

	
	public static class Patient{
		private int slNo;
		private String name;
		private String date,time;
		ObjectId p_id;
		public Patient(int slNo, String name, String date, String time, ObjectId m_id) {
			super();
			this.slNo = slNo;
			this.name = name;
			this.date = date;
			this.time = time;
			this.p_id = m_id;
		}
		public int getSlNo() {
			return slNo;
		}
		public void setSlNo(int slNo) {
			this.slNo = slNo;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public String getTime() {
			return time;
		}
		public void setTime(String time) {
			this.time = time;
		}
		public ObjectId getP_id() {
			return p_id;
		}
		public void setP_id(ObjectId m_id) {
			this.p_id = m_id;
		}
		
	}
}
