package patient;

import javafx.fxml.FXML;

import com.jfoenix.controls.JFXTimePicker;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import javafx.scene.control.Button;
import javafx.stage.Stage;

import com.jfoenix.controls.JFXButton;

import javafx.event.ActionEvent;

import com.jfoenix.controls.JFXDatePicker;

public class DateTimeController {
	@FXML
	private JFXDatePicker datePicker;
	@FXML
	private JFXTimePicker timePicker;
	@FXML
	private Button closeButton;
	@FXML
	private JFXButton reschedule;
	MongoClient mongoClient = new MongoClient("localhost", 27017);
	DB db = mongoClient.getDB("PLocalDB");// get the existing database DBCollection table=
	DBCollection table = db.getCollection("Patient");   
	// Event Listener on Button[#closeButton].onAction
	@FXML
	public void closeWindow(ActionEvent event) {
		Stage stage=(Stage)closeButton.getScene().getWindow();
		stage.close();
	}
	@FXML 
	void scheduler(ActionEvent e) {
		DBObject ob = new BasicDBObject();
		ob.put("_id", FullListController._id);
		DBCursor dbc = table.find(ob);
		BasicDBObject doc = new BasicDBObject();		
		doc.put("NextConsultationDate", datePicker.getValue().toString());	

		doc.put("NextConsultationTime",timePicker.getValue().toString() );	
		table.update(ob, new BasicDBObject("$set",doc));
		closeWindow(e);
	}
	
	
}
