package registration;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.bson.BSONObject;
import org.bson.Document;
import org.controlsfx.control.Notifications;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;

public class RegistrationController implements Initializable {
	ObservableList<String> maritals=FXCollections.observableArrayList("Married","Single","Divorced");
	ObservableList<String> bloodGroup=FXCollections.observableArrayList("A+","A-","B+","B-","O+","O-","AB+","AB-");
	ObservableList<String> employ=FXCollections.observableArrayList("Employed","Unemployed","Student");
	ObservableList<String> genderArray=FXCollections.observableArrayList("Male","Female","Other");

	@FXML
	    private Tab basic;
	  @FXML
	    private Tab address;

	  @FXML
	    private Tab relation;
	    @FXML
	    private JFXTextField age;
	    @FXML
	    private JFXComboBox<String> gender;
	    @FXML
	    private JFXTextField mobileno;

	    @FXML
	    private JFXTextField patientName;

	    @FXML
	    private JFXTextField height;

	    @FXML
	    private JFXTextField weight;

	    @FXML
	    private JFXComboBox<String> employment;

	    @FXML
	    private JFXTextField symptoms;

	    @FXML
	    private JFXTextField childrens;

	    @FXML
	    private JFXComboBox<String> blood;

	    @FXML
	    private JFXComboBox<String> marital;

	    

	  

	    @FXML
	    private JFXComboBox<String> headGender;

	    @FXML
	    private JFXTextField HeadAge;

	    @FXML
	    private JFXTextField headName;

	    @FXML
	    private JFXTextField headChildren;

	    @FXML
	    private JFXComboBox<String> headMarital;

	    @FXML
	    private JFXButton nextPageButton2;
	    @FXML
	    private JFXButton nextPageButton1;
	    @FXML
	    private JFXTextField village;

	    @FXML
	    private JFXTextField district;

	    @FXML
	    private JFXTextField houseName;

	    @FXML
	    private JFXTextField numberOfCases;

	    @FXML
	    private JFXTextField taluk;
	    @FXML
	    private JFXDatePicker nextDate;

	    @FXML
	    private JFXTimePicker nextTime;
	   

	    @FXML
	    private JFXTextField byAge;

	    @FXML
	    private JFXTextField byMobile;

	    @FXML
	    private JFXTextField byName;

	    @FXML
	    private JFXComboBox<String> byBlood;

	    @FXML
	    private JFXTextField byRelation;

	    @FXML
	    private JFXTextField byAddress;

	    @FXML
	    private JFXComboBox<String> byMarital;
	    

	    @FXML
	    private JFXComboBox<String> byGender;

	    @FXML
	    private JFXTextField byPin;

	    @FXML
	    private JFXTextField byTaluk;

	    @FXML
	    private JFXButton registerButton;
	    @FXML
	    private JFXTextField income;

	    @FXML
	    private Button closeButton;
	    DB db;
	    
	    @FXML
	    void register(ActionEvent event) {
			System.out.print("in Register");
			DateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

			LocalDate value=nextDate.getValue();
			
			LocalTime time=nextTime.getValue();
			System.out.println(value);			
			System.out.println(time);

	    	MongoClient mongoClient = new MongoClient("localhost",27017);
	    	DB db = mongoClient.getDB("PLocalDB"); 
	    	//db.createCollection("Patient");
	    	//if(!(d.collectionExists("Patient"))) {
	    	//d.createCollection("Patient", new BasicDBObject());}
	    	DBCollection table = db.getCollection("Patient");  
	    	BasicDBObject doc=new BasicDBObject();
	    	if(!patientName.getText().isEmpty()&&!mobileno.getText().isEmpty()) {
	    	System.out.println("in Register");
	    	doc.put("Name", patientName.getText());
	    	doc.put("Mobile No", mobileno.getText());
	    	doc.put("Age", age.getText());	    	
	    	doc.put("Childrens", childrens.getText());
	    	doc.put("Weight",weight.getText());
	    	doc.put("Height",height.getText());
	    	doc.put("symptoms",symptoms.getText());
	    	doc.put("Id",mobileno.getText());
	    	doc.put("Employment",employment.getValue());
	    	doc.put("Gender", gender.getValue());
	    	doc.put("MaritalStatus", marital.getValue());
	    	doc.put("BloodGroup", blood.getValue());
	    	doc.put("Time",new Date());
	    	doc.put("Annuel Income", income.getText());
	    	doc.put("NextConsultationDate", value.toString());
	    	doc.put("NextConsultationTime", time.toString());
	    	doc.put("IsConsultationCompleted", 0);

	    	doc.put("HeadName", headName.getText());
	    	doc.put("HeadAge", HeadAge.getText());
	    	doc.put("HeadGender", headGender.getValue());
	    	doc.put("HeadMarital Status", headMarital.getValue());
	    	doc.put("HeadChildrens", headChildren.getText());
	    	doc.put("House Name",houseName .getText());
	    	doc.put("District", district.getText());
	    	doc.put("Village", village.getText());
	    	doc.put("Taluk", taluk.getText());
	    	doc.put("NumberOfCases", numberOfCases.getText());
	    	
	    	doc.put("ByName", byName.getText());
	    	doc.put("ByMobile", byMobile.getText());
	    	doc.put("ByRelation", byRelation.getText());
	    	doc.put("ByAge", byAge.getText());
	    	doc.put("ByGender", byGender.getValue());
	    	doc.put("ByMarital Status", byMarital.getValue());
	    	doc.put("ByAddress", byAddress.getText());
	    	doc.put("ByBloodGroup", byBlood.getValue());
	    	doc.put("ByTaluk", taluk.getText());
	    	doc.put("Pin", byPin.getText());

	    	table.insert(doc);
	    	Stage stage=(Stage)registerButton.getScene().getWindow();
	    	stage.close();}
	    	else {
	    		Image img= new Image("/img/Alert1.png");
	    		Notifications notyf=Notifications.create()
						.title("")
						.text("Patient Name and Mobile.no are mandatory.")
						.graphic(new ImageView(img))
						.hideAfter(Duration.seconds(2))
						.position(Pos.CENTER); 
				notyf.darkStyle();
				notyf.show();
			}

	    }
	    
	    @FXML
	    void closeWindow(ActionEvent event) {
	    	Stage stage=(Stage)closeButton.getScene().getWindow();
			stage.close();
	    }

		@Override
		public void initialize(URL arg0, ResourceBundle arg1) {
			System.out.print("in initialize");
			gender.setItems(genderArray);
			byGender.setItems(genderArray);
			headGender.setItems(genderArray);
			employment.setItems(employ);
			marital.setItems(maritals);
			blood.setItems(bloodGroup);
			headMarital.setItems(maritals);
			byBlood.setItems(bloodGroup);
			byMarital.setItems(maritals);
		}

}
