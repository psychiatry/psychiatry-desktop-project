package registration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;



import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import javafx.scene.control.Alert;

public class DBConnection {

    MongoClient mongoClient;
    private DB db = null;
    private int highestSequenceNumber = 0;     // For Messaging

    static DBCollection table;
    public static int TableRowIndex = 0;
    public static String[][] arrayPatient;
    public static String[][] arrayPatientDetails;
    public static ArrayList patientPhoto = new ArrayList();
    
    public void runDBS() {
        try {
        	mongoClient = new MongoClient("localhost",27017);
	    	DB db = mongoClient.getDB("PLocalDB"); 
	    	DBCollection table= db.getCollection("Patient");  
        } catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Please start the server");
            alert.showAndWait();
            System.exit(0);
        }
    }
    public String[][] getPatient() {
        runDBS();
        int i = 0;
        SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
        BasicDBObject ob = new BasicDBObject();
        DBCursor cr = table.find(ob);
        arrayPatient = new String[cr.count()][7];
        while (cr.hasNext()) {
            ob = (BasicDBObject) cr.next();
            arrayPatient[i][0] = Integer.toString(i + 1);
            arrayPatient[i][1] = ob.getString("_id");
            arrayPatient[i][2] = ob.getString("Name");
            arrayPatient[i][3] = ob.getString("Gender");
            arrayPatient[i][4] = ob.getString("Age");
            arrayPatient[i][5] = sd.format(ob.get("Episode"));
            arrayPatient[i][6] = ob.getString("Time");
            arrayPatient[i][7] = ob.getString("Symptoms");
            i++;
        }
        cr.close();
        mongoClient.close();

        return arrayPatient;
    }
}