package medicine;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import patient.PatientController;

public class UpdateMedicine implements Initializable {
	ObservableList<String> freqncies=FXCollections.observableArrayList("0 - 0 - 0","0 - 0 - 1","0 - 1 - 0","0 - 1 - 1","1 - 1 - 1","1 - 0 - 1");
	ObservableList<String> foodConnections=FXCollections.observableArrayList("Before Food","After Food","No Connection");

    @FXML
    private JFXTextField medicineName;

    @FXML
    private JFXTextField dosage;

    @FXML
    private JFXComboBox<String> frequency;

    @FXML
    private JFXComboBox<String> foodConnection;

    @FXML
    private JFXTextField duration;

    @FXML
    private Button closeButton;

    @FXML
    private JFXButton update;
    
    MongoClient mongoClient = new MongoClient("localhost", 27017);
	DB db = mongoClient.getDB("PLocalDB");// get the existing database DBCollection table=
	DBCollection mtable = db.getCollection("Medicine");
	
    @FXML
    void closeWindow(ActionEvent event) {
    	Stage stage=(Stage)closeButton.getScene().getWindow();
		stage.close();
    }

    BasicDBObject qry=new BasicDBObject();
	//qry.put("_id",PatientController.id1);

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		frequency.setItems(freqncies);
		foodConnection.setItems(foodConnections);
		//BasicDBObject qry=new BasicDBObject();
		qry.put("_id",PatientController.id1);
		BasicDBObject bo=new BasicDBObject();
		DBCursor cr=mtable.find(qry);
		if(cr.hasNext()) {
			bo=(BasicDBObject) cr.next();
			medicineName.setText(bo.getString("MedicineName"));
			dosage.setText(bo.getString("Dosage"));
			frequency.setValue(bo.getString("Frequency"));
			foodConnection.setValue(bo.getString("FoodConnection"));
			duration.setText(bo.getString("Duration"));
			
			
		}
	}
	
	@FXML
	public void update() {
		System.out.println("in MedUpdate");
		BasicDBObject qry=new BasicDBObject();
		BasicDBObject doc = new BasicDBObject();
		qry.put("_id",PatientController.id1);
		BasicDBObject bo=new BasicDBObject();
		DBCursor cr=mtable.find(qry);
		doc.put("MedicineName",medicineName.getText());
		doc.put("Dosage", dosage.getText());
		doc.put("Frequency",frequency .getValue());
		doc.put("FoodConnection", foodConnection.getValue());
		doc.put("Duration", duration.getText());

		mtable.update(qry, new BasicDBObject("$set", doc));
		Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
		PatientController pc=new PatientController();
		//pc.refresh();
	}

}
