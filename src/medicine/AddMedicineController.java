package medicine;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import org.bson.types.ObjectId;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

import firstPage.FirstPageController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class AddMedicineController implements Initializable {
	ObservableList<String> foodConnections=FXCollections.observableArrayList("Before Food","After Food","No Connection");
	ObservableList<String> freqncies=FXCollections.observableArrayList("0","1");

	@FXML
    private JFXTextField medicineName;
    @FXML
    private JFXTextField dosage;
    @FXML
    private JFXComboBox<String> frequency1,frequency2,frequency3,foodConnection;
    @FXML
    private JFXTextField duration;
    @FXML
    private Button closeButton;
    @FXML
    private JFXButton addBtn;
    
    MongoClient mongoClient = new MongoClient("localhost", 27017);
	DB db = mongoClient.getDB("PLocalDB");// get the existing database DBCollection table=
	DBCollection table = db.getCollection("Medicine");
	
	
    @FXML
    void closeWindow(ActionEvent event) {
    	Stage stage=(Stage)closeButton.getScene().getWindow();
		stage.close();
    }
    
    
    //---------------------------
    @FXML
    public void aadMedicine(ActionEvent e) {
    	System.out.println("in addMedicine ");
		BasicDBObject doc=new BasicDBObject();
		doc.put("p_id",new ObjectId( FirstPageController.id));
		doc.put("MedicineName", medicineName.getText());
		doc.put("Dosage", dosage.getText());
		String fr=frequency1.getValue()+" - "+frequency2.getValue()+" - "+frequency3.getValue();
		doc.put("Frequency", fr);
		doc.put("FoodConnection", foodConnection.getValue());
		doc.put("Duration", duration.getText());
		doc.put("PrescriptionDate", new Date());
		table.insert(doc);
		
		Stage stage=(Stage)closeButton.getScene().getWindow();
		stage.close();
	}
   


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		frequency1.setItems(freqncies);
		frequency2.setItems(freqncies);
		frequency3.setItems(freqncies);
		foodConnection.setItems(foodConnections);
	}
	
	

}
