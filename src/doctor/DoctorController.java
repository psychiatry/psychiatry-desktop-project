package doctor;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import login.LoginController;

public class DoctorController implements Initializable {
	public String docId = LoginController.docId;
	@FXML
	private Tab profile;
	@FXML
	private Label Dname1;
	@FXML
	private Label Dname;
	@FXML
	private Label Dgender;
	@FXML
	private Label Dmobile;
	@FXML
	private Label Dage;
	@FXML
	private Label Dmarital;
	@FXML
	private Label pwd;
	@FXML
	private Label Daddress;
	@FXML
	private Label Dtaluk;
	@FXML
	private Label Dpo;
	@FXML
	private Label Ddistrict;
	@FXML
	private Label Dvillage;
	@FXML
	private Tab consultations;
	@FXML
	private Tab records;
	@FXML
	private Button closeButton;
	@FXML
	private Label dName;

	@FXML
	void closeWindow(ActionEvent e) {
		Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		MongoClient mongoClient = new MongoClient("localhost", 27017);
		DB db = mongoClient.getDB("PLocalDB");// get the existing database DBCollection table=
		DBCollection table = db.getCollection("Doctor");
		System.out.println("Connected");

		DBObject ob = new BasicDBObject();
		ob.put("_id", new ObjectId(docId));
		BasicDBObject bo = new BasicDBObject();
		DBCursor dbc = table.find(ob);
		if (dbc.hasNext()) {
			bo = (BasicDBObject) dbc.next();
			Dname1.setText(bo.getString("Name"));
			Dname.setText(bo.getString("Name"));
			Dmobile.setText(bo.getString("Mobile"));
			pwd.setText(bo.getString("Password"));
			Dgender.setText(bo.getString("Gender"));
			Ddistrict.setText(bo.getString("District"));
			Dmarital.setText(bo.getString("MaritalStatus"));
			Dvillage.setText(bo.getString("Village"));
			Daddress.setText(bo.getString("Address"));
			Dtaluk.setText(bo.getString("Taluk"));
			Dpo.setText(bo.getString("PO"));
			Dage.setText(bo.getString("Age"));
		}

	}

	// -------------------Update details Window----------
	@FXML
	void doctorUpdateWindow(ActionEvent event) {
		Stage primaryStage = new Stage();
		primaryStage.initStyle(StageStyle.UNDECORATED);
		// loadSpalsh();
		AnchorPane root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/doctor/DocUpdate.fxml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	@FXML
	public void refresh() {
		initialize(null, null);
	}
}
