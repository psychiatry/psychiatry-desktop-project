package doctor;

import java.net.URL;
import java.util.ResourceBundle;

import org.bson.types.ObjectId;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import login.LoginController;

public class DocUpdate implements Initializable {
	@FXML
	private JFXTextField dName;
	DoctorController dc = new DoctorController();
	@FXML
	private JFXTextField dGender;

	@FXML
	private JFXTextField dMobile;

	@FXML
	private JFXTextField dAge;

	@FXML
	private JFXTextField dMarital;

	@FXML
	private JFXTextField dPassword;

	@FXML
	private JFXTextField dAddress;

	@FXML
	private JFXTextField dTaluk;

	@FXML
	private JFXTextField dPO;

	@FXML
	private JFXTextField dVillage;

	@FXML
	private JFXTextField dDistrict;

	@FXML
	private JFXButton updateBtn;

	@FXML
	private Button closeButton;
	String id = LoginController.docId;

	// -----------Close---------------------------
	@FXML
	void closeWindow(ActionEvent event) {
		Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
	}

	MongoClient mongoClient = new MongoClient("localhost", 27017);
	DB db = mongoClient.getDB("PLocalDB");// get the existing database DBCollection table=
	DBCollection table = db.getCollection("Doctor");

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		System.out.println("Connected");

		DBObject ob = new BasicDBObject();
		ob.put("_id", new ObjectId(id));
		BasicDBObject bo = new BasicDBObject();
		DBCursor dbc = table.find(ob);
		if (dbc.hasNext()) {
			bo = (BasicDBObject) dbc.next();
			dMobile.setText(bo.getString("Mobile"));
			dName.setText(bo.getString("Name"));
			dPassword.setText(bo.getString("Password"));
			dAddress.setText(bo.getString("Address"));
			dAge.setText(bo.getString("Age"));
			dGender.setText(bo.getString("Gender"));
			dMarital.setText(bo.getString("MaritalStatus"));
			dTaluk.setText(bo.getString("Taluk"));
			dVillage.setText(bo.getString("Village"));
			dPO.setText(bo.getString("PO"));
			dDistrict.setText(bo.getString("District"));
		}

	}

//--------------------Details Updation----------
	@FXML
	private void updateDetails(ActionEvent event) {
		System.out.println("in Update");
		BasicDBObject doc = new BasicDBObject();
		DBObject ob = new BasicDBObject();
		ob.put("_id", new ObjectId(id));
		DBCursor dbc = table.find(ob);
		
		
		doc.put("Name", dName.getText());
		doc.put("Age", dAge.getText());		
		doc.put("Password", dPassword.getText());
		doc.put("Gender", dGender.getText());
		doc.put("MaritalStatus", dMarital.getText());
		doc.put("Address", dAddress.getText());
		doc.put("Taluk", dTaluk.getText());
		doc.put("Village", dVillage.getText());
		doc.put("District", dDistrict.getText());
		doc.put("PO", dPO.getText());

		table.update(ob, new BasicDBObject("$set", doc));
		System.out.println("in Update");

		Stage stage = (Stage) closeButton.getScene().getWindow();

		stage.close();
	}
}
