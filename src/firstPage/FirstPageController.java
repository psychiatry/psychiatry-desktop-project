package firstPage;
import patient.*;
import registration.*;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.Timer;

import javax.swing.text.DateFormatter;

import org.bson.types.ObjectId;

import com.jfoenix.controls.JFXButton;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import login.LoginController;

public class FirstPageController implements Initializable{
	public static String id;
	
	@FXML
	private Button closeButton;
	@FXML
    private TextField serchbox;
	
	@FXML
	private JFXButton b1;
	@FXML
	private JFXButton b2;
	@FXML
	private JFXButton b3;
	@FXML
	private JFXButton b4;
	@FXML
	private JFXButton b5;
	@FXML
	private JFXButton b6;
    @FXML
    private Button search;
    @FXML
    private MenuButton dropButton;
    
    @FXML
    private TableView<Person> firsrPageTable;
    @FXML
    private TableColumn<Person, Integer> slNo;
    @FXML
    private TableColumn<Person, JFXButton> view;
    @FXML
    private TableColumn<Person, String> patientName;
    @FXML
    private TableColumn<Person, String> firstAdmission;

    @FXML
    private TableColumn <Person,String>episode;
    @FXML
    private TableColumn<Person, String> time;
    @FXML
    private TableColumn<Person, String> symptoms;
    @FXML
    private TableColumn<Person, String> pID;
    @FXML
    private TableColumn gender;
    @FXML
    private TableColumn age;
    @FXML
    private JFXButton completeBtn;
    @FXML
    private JFXButton patientDetails;
    @FXML
    private Label docName,date;

    
    
    private int i=1;
    private int j=1;

    
   private final ObservableList<Person> list= FXCollections.observableArrayList();
   String[][] datas;
   MongoClient mongoClient = new MongoClient("localhost",27017); 
	  DB db =mongoClient.getDB("PLocalDB");
	  DBCollection table=db.getCollection("Patient"); 		  
	  DBCollection docColl=db.getCollection("Doctor"); 
	  
    @Override
	public void initialize(URL arg0, ResourceBundle arg1) {
    	 DateFormat formater1 = new SimpleDateFormat("dd/MM/yyyy");
		 LocalDate dat= LocalDate.now(); 
		 DateTimeFormatter formater2=DateTimeFormatter.ofPattern("dd/MM/YYYY");
		 System.out.println(formater2.format(dat)); date.setText(formater2.format(dat));
		 

		  
		  BasicDBObject bo= new BasicDBObject();
		  DBObject ob1 = new BasicDBObject();		  
		  DBObject ob2 = new BasicDBObject();
		  DBObject ob3 = new BasicDBObject();
		  	ob2.put("NextConsultationTime", 1);
			ob1.put("NextConsultationDate", dat.toString());
			
		  DBCursor cursor=table.find(ob1).sort(ob2);

		  
		  while (cursor.hasNext()) { 
			  String id="P0"+Integer.toString(j++);
			  //System.out.println(bo.getString("_id"));
			  bo = (BasicDBObject) cursor.next();
			  if(bo.getInt("IsConsultationCompleted")==0) {
				 System.out.println(formater1.format(bo.getDate("Time")));

			  list.add(new Person(Integer.toString(i++),bo.getString("_id"),id, bo.getString("Name"),bo.getString("Gender") , bo.getString("Age"),  bo.getString("NextConsultationTime"),bo.getString("symptoms")));
			 }
			  }
	
		
		slNo.setCellValueFactory(new PropertyValueFactory<>("slNo"));
		pID.setCellValueFactory(new PropertyValueFactory<>("pID"));
		patientName.setCellValueFactory(new PropertyValueFactory<>("patientName"));
		gender.setCellValueFactory(new PropertyValueFactory<>("gender"));
		age.setCellValueFactory(new PropertyValueFactory<>("age"));
		episode.setCellValueFactory(new PropertyValueFactory<>("episode"));
		time.setCellValueFactory(new PropertyValueFactory<>("time"));
		symptoms.setCellValueFactory(new PropertyValueFactory<>("symptoms"));
		view.setCellValueFactory(new PropertyValueFactory<>("btn"));

		
		firsrPageTable.setItems(list);
		
		
		//--------------------------
		DBObject ob = new BasicDBObject();
		ob.put("_id", new ObjectId(LoginController.docId));
		BasicDBObject bobj=new BasicDBObject();
		DBCursor dbc=docColl.find(bobj);
		if (dbc.hasNext()) {			
			bobj = (BasicDBObject) dbc.next();
			docName.setText(bobj.getString("Name"));
		} 
		//Timer ti=new Timer(1000,new Listener())
	}
    
//------------------VC request----------------
    @FXML
    void vc(ActionEvent e) throws IOException {
    	
    		String url="https://etherapy.in/vcroom/?userType=&userId=#room3";
    		Runtime runtime = Runtime.getRuntime();
            runtime.exec("/snap/bin/chromium -new-window " + url);

            }
	
    	
    
//-----------Close---------------------------
    
	@FXML
	void closeWindow(ActionEvent e) {
		Stage stage=(Stage)closeButton.getScene().getWindow();
		stage.close();
	}
	
	
	
//-------------------Registration------------------
	@FXML
	void registrationPage(ActionEvent e) {
		Stage primaryStage=new Stage();
		primaryStage.initStyle(StageStyle.UNDECORATED);
		//loadSpalsh();
		AnchorPane root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/registration/Registration.fxml"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Scene scene = new Scene(root);
		//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		//scene.setRoot(root);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	
	
	
//-------------------------Logout----------------------
	@FXML
	void logout(ActionEvent event) {
		Stage stage=(Stage)dropButton.getScene().getWindow();
		stage.close();
		Stage stage2=new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/login/Login.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		//scene.setRoot(root);
		stage2.initStyle(StageStyle.UNDECORATED);
		stage2.setScene(scene);
		stage2.show();
		System.out.print("fkyfhjhyfb");
		
		
	}
	
	
	
	
	
//------------------------Account view-----------------
	@FXML
	void myAccount() {
		Stage stage2=new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/doctor/Doctor.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		//scene.setRoot(root);
		stage2.initStyle(StageStyle.UNDECORATED);
		stage2.setScene(scene);
		stage2.show();
	}
	
	
	
//----------------------------FeedbackWindow-------------------
	@FXML
	void feedback(ActionEvent event) {
		Stage stage=new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/feedback/FeedBack.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		//scene.setRoot(root);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setScene(scene);
		stage.show();
	}
	
	
	
	
	
	
	//------------------------Patient Details Window--------------------
	@FXML
	void patientDetails(ActionEvent event) {
		id=firsrPageTable.getSelectionModel().getSelectedItem().getPId();
		
		Stage stage=new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/patient/Patient.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		//scene.setRoot(root);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setScene(scene);
		stage.show();
		
	}
	
	//------------------------Comleted---------------------
	@FXML
	void completed(ActionEvent event) {
		String _id=firsrPageTable.getSelectionModel().getSelectedItem().getPId();
		BasicDBObject idob=new BasicDBObject();
		BasicDBObject id=new BasicDBObject();
		id.put("_id", new ObjectId(_id));
		idob.put("IsConsultationCompleted",1);
		table.update(id, new BasicDBObject("$set",idob));
		firsrPageTable.getItems().removeAll(list); 
		i=1;j=1;
		initialize(null, null);
	}
	
	
	//-----------------------refresh-----------------
	@FXML 
	void refresh(ActionEvent e) {
		firsrPageTable.getItems().removeAll(list); 
		i=1;j=1;
		initialize(null, null);
	}
	
	
	//--------------fullList--------------
	@FXML
	 void fullList(ActionEvent e) {
		
		Stage stage=new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/patient/FullList.fxml"));
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		Scene scene = new Scene(root);
		//scene.setRoot(root);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setScene(scene);
		stage.show();
		

	}
	

    
public static class Person {
	private final SimpleStringProperty slNo;
    private final SimpleStringProperty patientName;
    private final SimpleStringProperty gender;
    private final SimpleStringProperty age;
    private final SimpleStringProperty episode;
    private final SimpleStringProperty time;
    private final String symptoms;
    private final SimpleStringProperty pID;
    private final JFXButton btn;
   
    
    
    
    private Person(String Sno, String Pid, String episodeNo,String Name, String Gender,String Age, String Date, String sym ) {
        this.slNo = new SimpleStringProperty(Sno);
        this.pID = new SimpleStringProperty(Pid);
        this.patientName = new SimpleStringProperty(Name);
        this.gender = new SimpleStringProperty(Gender);
        this.age = new SimpleStringProperty(Age);
        this.episode = new SimpleStringProperty(episodeNo);
        this.time = new SimpleStringProperty(Date);
        this.symptoms = sym;
        Image image =new Image(getClass().getResourceAsStream("/img/eye.png")); 
        this.btn=new JFXButton();
       // image = new Image(getClass().getResourceAsStream("eye-icon-png.png"));
        btn.setGraphic(new ImageView(image));
    }
    
    public String getSymptoms() {
		return symptoms;
    }

	public SimpleStringProperty getpID() {
		return pID;
	}

	public JFXButton getBtn() {
		return btn;
	}

	public String getSlNo() {
        return slNo.get();
    }

    public void setSlNo(String Sno) {
        slNo.set(Sno);
    }

    public String getPId() {
        return pID.get();
    }

    public void setPId(String Pid) {
        pID.set(Pid);
    }

    public String getPatientName() {
        return patientName.get();
    }

    public void setPatientName(String Name) {
        patientName.set(Name);
    }
    
    public String getAge() {
        return age.get();
    }

    public void setAge(String Age) {
        age.set(Age);
    }
    
    public String getGender() {
        return gender.get();
    }

    public void setGender(String Gender) {
        gender.set(Gender);
    }
    
    public String getTime() {
        return time.get();
    }

    public void setTime(String Date) {
        time.set(Date);
    }
    
    public String getEpisode() {
        return episode.get();
    }

    public void setEpisode(String Mobile) {
        episode.set(Mobile);
    }
    
   
	
}
}

